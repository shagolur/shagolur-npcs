package fr.shagolur.npcs.events;

import fr.shagolur.core.players.SPlayer;
import fr.shagolur.npcs.npcs.SNPC;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class ShagolurPlayerSpeakNPC extends Event implements Cancellable {
	
	private final SPlayer player;
	private final SNPC npc;
	private boolean cancelled;
	
	public ShagolurPlayerSpeakNPC(SPlayer player, SNPC npc) {
		this.player = player;
		this.npc = npc;
		cancelled = false;
	}
	
	public SPlayer getPlayer() {
		return player;
	}
	
	public SNPC getNPC() {
		return npc;
	}
	
	private static final HandlerList handlers = new HandlerList();
	@Override public @NotNull HandlerList getHandlers() {
		return handlers;
	}
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	@Override
	public boolean isCancelled() {
		return cancelled;
	}
	
	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}
