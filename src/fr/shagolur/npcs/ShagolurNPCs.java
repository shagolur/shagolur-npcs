package fr.shagolur.npcs;

import fr.shagolur.core.ShagolurModule;
import fr.shagolur.npcs.commands.DebugExtension;
import fr.shagolur.npcs.commands.NpcsCommand;
import fr.shagolur.npcs.listeners.NpcInteractListener;
import fr.shagolur.npcs.npcs.NpcsManager;
import org.bukkit.Bukkit;
import org.jetbrains.annotations.NotNull;

public class ShagolurNPCs extends ShagolurModule {
	
	private static ShagolurNPCs MODULE;
	public static ShagolurNPCs module() {
		return MODULE;
	}
	
	private NpcsManager npcs;
	
	@Override
	public void onEnable() {
		MODULE = this;
		// Components
		npcs = new NpcsManager();
		// Events
		Bukkit.getPluginManager().registerEvents(new NpcInteractListener(), this);
		// Commands
		new NpcsCommand("sg.npcs");
		// Extension
		new DebugExtension();
	}
	
	public void onDisable() {
		npcs.purge();
		npcs.saveAll();
	}
	
	public static @NotNull NpcsManager getNpcs() {
		return MODULE.npcs;
	}
	
}
