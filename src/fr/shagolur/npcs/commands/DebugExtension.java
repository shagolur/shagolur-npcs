package fr.shagolur.npcs.commands;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.commands.CommandSenderHandler;
import fr.shagolur.core.items.Data;
import fr.shagolur.core.utils.SerializationUtils;
import fr.shagolur.npcs.ShagolurNPCs;
import fr.shagolur.npcs.npcs.NpcData;
import org.bukkit.command.CommandSender;

public class DebugExtension implements CommandSenderHandler {
	
	public DebugExtension() {
		Shagolur.getDebugCommand().addDataExtension(
				"npc",
				() -> ShagolurNPCs.getNpcs().getStream().map(NpcData::getId).toList(),
				this::handleCommand
		);
	}
	
	public void handleCommand(String npcId, CommandSender sender) {
		NpcData npc = ShagolurNPCs.getNpcs().get(npcId);
		if(npc == null) {
			sendError(sender, "Npc inconnu : \""+npcId+"\".");
			return;
		}
		sendSuccess(sender, "------- [NPC DATA {"+npc.getDisplayName()+"§a}] -------");
		sendInfo(sender, "BddID : §b" + npc.getId());
		sendInfo(sender, "Position : " + SerializationUtils.niceLocation(npc.getLocation(), "§e"));
		sendInfo(sender, "Equipment : " + (npc.getEquipment().isEmpty()?"§7none":""));
		npc.getEquipment().forEach((key, value) -> sendInfo(sender, " - " + key + " : " + value.getName()));
		sendInfo(sender, "------------------------------------------");
	}
	
}
