package fr.shagolur.npcs.commands;

import fr.shagolur.core.commands.JamArg;
import fr.shagolur.core.commands.JamCommand;
import fr.shagolur.npcs.ShagolurNPCs;
import fr.shagolur.npcs.npcs.NpcData;
import fr.shagolur.npcs.npcs.SNPC;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class NpcsCommand extends JamCommand {
	
	public NpcsCommand(String command) {
		super(command, ShagolurNPCs.module());
		
		addArgument(0, "tp", "tphere", "dialog", "equipment");
		
		addArgumentIf(1, new JamArg[] {new JamArg(0, "tp", "tphere")},
				() -> ShagolurNPCs.getNpcs().getStream()
						.map(NpcData::getId)
						.toList()
		);
		
	}
	
	@Override
	public void onCommand(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
		//SPlayer player = Shagolur.getPlayersManager().getPlayer((Player)sender);
		
		if(args.length < 2) {
			sendError(sender, "/"+label+" [tp|tphere|dialog|equipment]");
			return;
		}
		Player player = (Player) sender;
		if(args[0].equals("tp")) {
			SNPC npc = ShagolurNPCs.getNpcs().getNPC(args[1]);
			if(npc == null) {
				sendError(sender, "Aucun npc ne correspond à l'id \""+args[1]+"\".");
				return;
			}
			if(npc.getEntity() == null)
				player.teleport(npc.getData().getLocation());
			else
				player.teleport(npc.getEntity());
			player.playSound(player.getLocation(), Sound.ITEM_CHORUS_FRUIT_TELEPORT, 1, 1);
			return;
		}
		if(args[0].equals("tphere")) {
			SNPC npc = ShagolurNPCs.getNpcs().getNPC(args[1]);
			if(npc == null) {
				sendError(sender, "Aucun npc ne correspond à l'id \""+args[1]+"\".");
				return;
			}
			npc.teleport(player);
			npc.save();
			player.playSound(player.getLocation(), Sound.ITEM_CHORUS_FRUIT_TELEPORT, 1, 1);
			return;
		}
		
		
		sendError(sender, "Sous commande inconnue.");
	}
	
	@Override
	public CommandExecutors getAllowedExecutors() {
		return CommandExecutors.PLAYER;
	}
}
