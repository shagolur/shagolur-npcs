package fr.shagolur.npcs.utils;

import dev.sergiferry.playernpc.api.NPC;
import org.bukkit.inventory.EquipmentSlot;

public final class NpcUtils {
	private NpcUtils() {}
	
	public static NPC.Slot slotBukkitToNpc(EquipmentSlot slot) {
		return switch (slot) {
			case HEAD -> NPC.Slot.HELMET;
			case CHEST -> NPC.Slot.CHESTPLATE;
			case LEGS -> NPC.Slot.LEGGINGS;
			case FEET -> NPC.Slot.BOOTS;
			case HAND -> NPC.Slot.MAINHAND;
			case OFF_HAND -> NPC.Slot.OFFHAND;
		};
	}
	
}
