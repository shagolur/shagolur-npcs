package fr.shagolur.npcs.listeners;

import dev.sergiferry.playernpc.api.NPC;
import fr.shagolur.core.Shagolur;
import fr.shagolur.core.players.SPlayer;
import fr.shagolur.npcs.ShagolurNPCs;
import fr.shagolur.npcs.events.ShagolurPlayerSpeakNPC;
import fr.shagolur.npcs.npcs.SNPC;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class NpcInteractListener implements Listener {
	
	@EventHandler
	public void playerInteractNpc(NPC.Events.Interact e) {
		if(e.isCancelled())
			return;
		if(e.isLeftClick()) {
			//TODO réaction du NPC en cas de clic gauche / attaque
			return;
		}
		Bukkit.broadcastMessage("player ("+e.getPlayer().getName()+") CLICKED on npc "+e.getNPC().getSimpleCode());
		SPlayer player = Shagolur.getPlayersManager().getPlayer(e.getPlayer());
		SNPC npc = ShagolurNPCs.getNpcs().getNPC(e.getNPC());
		
		// Publish event
		ShagolurPlayerSpeakNPC event = new ShagolurPlayerSpeakNPC(player, npc);
		Bukkit.getPluginManager().callEvent(event);
		
		// Cancel eventually
		e.setCancelled(event.isCancelled());
	}
	
}
