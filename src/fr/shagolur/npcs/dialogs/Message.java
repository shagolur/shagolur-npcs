package fr.shagolur.npcs.dialogs;

import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.players.SPlayer;
import org.bukkit.ChatColor;

import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public record Message(String unformatted) {
	
	private static final Pattern PATTERN_VARIABLE = Pattern.compile("%[A-Za-z_]+");
	
	private static final Map<String, Function<SPlayer, Object>> FORMATS = Map.of(
			"player", SEntity::getName,
			"name", SEntity::getName,
			"level", SPlayer::getLevel,
			"exp", SPlayer::getExp
	);
	
	public String get(SPlayer target) {
		if( ! target.exists())
			return ChatColor.translateAlternateColorCodes('&', unformatted);;
		String message = unformatted;
		Matcher m = PATTERN_VARIABLE.matcher(message);
		while (m.find()) {
			String g = m.group();
			String varName = g.substring(1);
			if(FORMATS.containsKey(varName))
				 message = message.replace(g, FORMATS.get(varName).toString());
		}
		return ChatColor.translateAlternateColorCodes('&', message);
	}
	
}
