package fr.shagolur.npcs.dialogs;

import fr.shagolur.core.players.SPlayer;

import java.util.List;

public class Conversation {
	
	private final List<Message> messages;
	
	Conversation(String message) {
		messages = List.of(new Message(message));
	}
	
	public Conversation(List<String> messages) {
		this.messages = messages.stream()
				.map(Message::new)
				.toList();
	}
	
	public List<String> getForPlayer(SPlayer player) {
		return messages.stream()
				.map(m -> m.get(player))
				.toList();
	}
	
	public String[] serialize() {
		return (String[]) messages.stream().map(Message::unformatted).toArray();
	}
	
}
