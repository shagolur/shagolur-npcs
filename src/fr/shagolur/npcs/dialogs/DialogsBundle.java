package fr.shagolur.npcs.dialogs;

import fr.shagolur.core.players.SPlayer;
import fr.shagolur.core.utils.Utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

public class DialogsBundle {
	
	private final static Conversation NO_CONVERSATION = new Conversation("Je boude.");
	
	private final Set<Conversation> defaultConversations = new HashSet<>();
	private final Map<Predicate<SPlayer>, Conversation> conditionalConversations = new HashMap<>();
	
	public void setDefaultConversations(String[][] messages) {
		defaultConversations.clear();
		for(String[] conversation : messages) {
			Conversation conv = new Conversation(List.of(conversation));
			defaultConversations.add(conv);
		}
	}
	
	public String[][] serializeDefaultConversation() {
		return (String[][]) defaultConversations.stream().map(Conversation::serialize).toArray();
	}
	
	public Conversation getConversation(SPlayer player) {
		// Test all conditional elements
		for(Predicate<SPlayer> predicate : conditionalConversations.keySet()) {
			if(predicate.test(player))
				return conditionalConversations.get(predicate);
		}
		// If none works, send a random element of the default set.
		if(defaultConversations.isEmpty())
			return NO_CONVERSATION;
		return defaultConversations
				.stream()
				.skip(Utils.randInt(0, conditionalConversations.size()-1))
				.findFirst()
				.orElse(null);
	}
	
}
