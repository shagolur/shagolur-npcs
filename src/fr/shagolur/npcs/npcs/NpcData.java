package fr.shagolur.npcs.npcs;

import fr.shagolur.core.items.ItemData;
import fr.shagolur.core.storage.Serializer;
import fr.shagolur.core.storage.Storable;
import fr.shagolur.npcs.dialogs.DialogsBundle;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.inventory.EquipmentSlot;

import java.util.HashMap;
import java.util.Map;

public class NpcData implements Storable {
	
	private final String id;
	private Location location;
	private String name, texture, signature;
	private boolean inBDD;
	private final Map<EquipmentSlot, ItemData> equipment;
	
	private final DialogsBundle dialogs = new DialogsBundle();;
	
	public NpcData(String id, Location location) {
		inBDD = false;
		// Common
		this.id = id;
		this.name = "&e" + id;
		this.location = location;
		this.texture = "";
		this.signature = "";
		equipment = new HashMap<>();
	}
	
	public NpcData(String id, String name, String signature, String texture,
	               Location location, Map<EquipmentSlot, ItemData> equipment, String[][] conversations) {
		inBDD = true;
		// Common
		this.id = id;
		this.name = name;
		this.signature = signature;
		this.texture = texture;
		this.location = location;
		this.equipment = equipment;
		
		dialogs.setDefaultConversations(conversations);
	}
	
	public DialogsBundle getDialogs() {
		return dialogs;
	}
	
	public String getId() {
		return id;
	}
	
	String getName() {
		return name;
	}
	public String getDisplayName() {
		return ChatColor.translateAlternateColorCodes('&', name);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Map<EquipmentSlot, ItemData> getEquipment() {
		return equipment;
	}
	
	public String getSkinTexture() {
		return texture;
	}
	
	public void setSkin(String signature, String texture) {
		this.signature = signature;
		this.texture = texture;
	}
	
	public String getSkinSignature() {
		return signature;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void setLocation(Location location) {
		this.location = location;
	}
	
	@Override
	public Object storageID() {
		return id;
	}
	
	@Override
	public Serializer<?> serialize() {
		return new NpcSerialized(this);
	}
	
	@Override
	public boolean alreadyInBDD() {
		return inBDD;
	}
	
	@Override
	public void declarePersistentInBDD() {
		inBDD = true;
	}
}
