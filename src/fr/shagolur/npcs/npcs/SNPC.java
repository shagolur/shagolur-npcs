package fr.shagolur.npcs.npcs;

import dev.sergiferry.playernpc.api.NPC;
import dev.sergiferry.playernpc.api.NPCLib;
import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.entities.SEntityType;
import fr.shagolur.npcs.ShagolurNPCs;
import fr.shagolur.npcs.utils.NpcUtils;
import org.bukkit.inventory.EquipmentSlot;

public class SNPC extends SEntity {
	
	private final NpcData bddData;
	private final NPC.Global apiNPC;
	
	
	public SNPC(NpcData data) {
		super(SEntityType.NPC);
		this.bddData = data;
		apiNPC = NPCLib.getInstance().generateGlobalNPC(ShagolurNPCs.module(), data.getId(), data.getLocation());
		apiNPC.setText(data.getDisplayName());
		apiNPC.setSkin(data.getSkinTexture(), data.getSkinSignature());
		apiNPC.setShowOnTabList(false);
		apiNPC.setAutoCreate(true);
		apiNPC.setAutoShow(true);
		updateEquipment();
	}
	
	public NpcData getData() {
		return bddData;
	}
	
	public NPC getApiNPC() {
		return apiNPC;
	}
	
	public void updateEquipment() {
		apiNPC.clearEquipment();
		for(EquipmentSlot slot : bddData.getEquipment().keySet()) {
			apiNPC.setItem(NpcUtils.slotBukkitToNpc(slot), bddData.getEquipment().get(slot).toItemStack());
		}
	}
	
	public void save() {
		//TODO
	}
	
}
