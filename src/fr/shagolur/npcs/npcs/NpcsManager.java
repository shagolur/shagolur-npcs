package fr.shagolur.npcs.npcs;

import dev.sergiferry.playernpc.api.NPC;
import fr.shagolur.core.common.Purgeable;
import fr.shagolur.core.storage.PathProvider;
import fr.shagolur.core.storage.StorableManager;
import org.bukkit.ChatColor;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public class NpcsManager extends StorableManager<NpcData> implements Purgeable {
	
	private final Map<String, SNPC> npcs = new HashMap<>();
	
	public NpcsManager() {
		// Set default data
		NPC.Attributes.setDefaultCollidable(false);
		NPC.Attributes.setDefaultFollowLookType(NPC.FollowLookType.NEAREST_ENTITY);
		NPC.Attributes.setDefaultInteractCooldown(20L);
		NPC.Attributes.setDefaultShowOnTabList(false);
		NPC.Attributes.setDefaultGlowingColor(ChatColor.YELLOW);
		//Spawn npcs
		super.collection.forEach(nData -> {
			SNPC npc = new SNPC(nData);
			npcs.put(nData.getId(), npc);
		});
	}
	
	public @Nullable SNPC getNPC(NPC npc) {
		if(npc == null)
			return null;
		return npcs.get(npc.getCode());
	}
	public @Nullable SNPC getNPC(String code) {
		return npcs.get(code);
	}
	
	@Override
	protected PathProvider<NpcData> getProvider() {
		return new NpcsPathProvider();
	}
	
	@Override
	protected String getStoredTypeName() {
		return "npcs";
	}
	
	@Override
	public void purge() {
		//TODO purge npcs
	}
	
	public void save(SNPC npc) {
		super.storage.saveUnique(NpcData.class, npc.getData());
	}
	public void saveAll() {
		npcs.values().forEach(this::save);
	}
}
