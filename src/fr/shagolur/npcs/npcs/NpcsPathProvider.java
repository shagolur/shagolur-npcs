package fr.shagolur.npcs.npcs;

import fr.shagolur.core.storage.PathProvider;
import fr.shagolur.core.storage.Serializer;

import java.util.Arrays;

class NpcsPathProvider implements PathProvider<NpcData> {
	
	private final static String PATH = "/api/npcs/";
	
	@Override
	public Class<NpcData> getHandledClass() {
		return NpcData.class;
	}
	
	@Override
	public Class<? extends Serializer<NpcData>> getSerializerClass() {
		return NpcSerialized.class;
	}
	
	@Override
	public String getPath(Object... params) {
		if(params.length == 0)
			return PATH;
		if(params[0] instanceof SNPC)
			return PATH + ((SNPC)params[0]).getData().getId();
		if(params[0] instanceof NpcData)
			return PATH + ((NpcData)params[0]).getId();
		throw new IllegalArgumentException("Unknown params : "+ Arrays.toString(params) + " for provider:npcs. Expected [SNPC/NpcData].");
	}
}
