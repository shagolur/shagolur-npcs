package fr.shagolur.npcs.npcs;

import com.google.gson.annotations.SerializedName;
import fr.shagolur.core.Shagolur;
import fr.shagolur.core.entities.SEntityEquipment;
import fr.shagolur.core.items.ItemData;
import fr.shagolur.core.storage.Serializer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.inventory.EquipmentSlot;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NpcSerialized implements Serializer<NpcData> {
	
	public String identifier;
	public String name;
	
	public String skin_signature;
	public String skin_texture;
	
	public String location_world;
	public double location_x;
	public double location_y;
	public double location_z;
	
	@SerializedName("mobequipment_set") public SEntityEquipment.EquipementSerialized[] equipment;
	@SerializedName("conversations") public String[][] defaultConversations;
	
	public NpcSerialized() {}
	public NpcSerialized(NpcData data) {
		this.identifier = data.getId();
		this.name = data.getName().replace('§', '&');
		this.skin_signature = data.getSkinSignature();
		this.skin_texture = data.getSkinTexture();
		this.location_world = data.getLocation().getWorld().getName();
		this.location_x = data.getLocation().getX();
		this.location_y = data.getLocation().getY();
		this.location_z = data.getLocation().getZ();
		this.equipment = (SEntityEquipment.EquipementSerialized[]) data.getEquipment().entrySet()
				.stream()
				.map(SEntityEquipment.EquipementSerialized::new)
				.toArray();
		this.defaultConversations = data.getDialogs().serializeDefaultConversation();
	}
	
	@Override
	public NpcData deserialize() {
		World world = Bukkit.getWorld(location_world);
		if(world == null) {
			Shagolur.error("[NPC] Npc '"+identifier+"' has unknown world : \""+location_world+"\".");
			return null;
		}
		Location loc = new Location(world, location_x, location_y, location_z);
		Map<EquipmentSlot, ItemData> equipment = Stream.of(this.equipment)
				.filter(eqp -> eqp.identifier != null)
				.map(SEntityEquipment.EquipementSerialized::serialize)
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		return new NpcData(identifier, name, skin_signature, skin_texture, loc, equipment, defaultConversations);
	}
}
